module.exports = {
  root: true,
  extends: ['@react-native-community', 'airbnb'],
  rules: {
    'prettier/prettier': 0,
    'react/jsx-filename-extension': 0,
    'react/prop-types': 0,
    'object-curly-newline': 0,
  },
};
